module funp
#(parameter ROWS = 3,
  parameter		NUM_POINTS = 2,
  parameter		 COLUMNS = 3)


(
	input			[31:0] V [0:ROWS-1][0:COLUMNS-1],
	input			[31:0] q [0:ROWS-1],
	input			[7:0] o [0:COLUMNS-1],
	input			[31:0] Uinv [0:ROWS-1][0:COLUMNS-1],
	input			[31:0] Winv [0:ROWS-1][0:COLUMNS-1],
	input			[31:0] S [0:COLUMNS-1],
	input			reset,
	input			CLK,
	input			go,
	output reg			[31:0] counter,
	output reg			[31:0] result [0:NUM_POINTS-1][0:COLUMNS-1]
);



localparam	INIT 	= 5'd0;
localparam	reset_i 	= 5'd1;	
localparam	increment_i = 5'd2;
localparam	increment_j = 5'd3;
localparam	calc_primeDiagonals = 5'd4;
localparam	calc_qhat_wprime = 5'd5;
localparam	calc_qint_qsummand 	= 5'd6;	
localparam	init_innerproduct 	= 5'd7;	
localparam	innerproduct 	= 5'd8;
localparam	inner_lastDiagonal 	= 5'd9;
localparam	inner_lastDiagonal_mod 	= 5'd10;
localparam	add_inner 	= 5'd11;
localparam	init_outerproduct 	= 5'd12;
localparam	outerproduct 	= 5'd13;
localparam	add_outer 	= 5'd14;
localparam	make_j_max 	= 5'd15;
localparam	check_next_vector 	= 5'd16;
localparam	check_finish 	= 5'd17;
localparam	END 	= 5'd18;
localparam	IDLE 	= 5'd19;
reg	[4:0]	current_state;



reg signed [7:0] j;
reg signed [7:0] i;
reg signed [7:0] point_count;


reg [7:0] lastDiagonal;
reg signed [31:0] qhat [0:ROWS-1];
reg signed [31:0] qint [0:ROWS-1];
reg signed [31:0] qsummand [0:ROWS-1];
reg [31:0] v_inner [0:COLUMNS-1];
reg [31:0] innerRes [0:COLUMNS-1];
reg [31:0] outerRes [0:COLUMNS-1];
reg signed [31:0] Wprime [0:ROWS-1][0:COLUMNS-1];
reg signed [7:0] primeDiagonals [0:COLUMNS-1];
reg signal_wprime_qhat;
reg signal_qint_qsummand;
reg signal_primeDiagonals;
reg signal_innerProduct;
reg signal_outerProduct;
reg signed [31:0] inner;
reg signed [31:0] outer;



//State Register & other registers
always@(posedge CLK, posedge reset)
	begin
	if(reset)
		begin
			current_state <= IDLE;
			lastDiagonal <= S[ROWS-1];
			signal_wprime_qhat <= 1'b0;
			signal_qint_qsummand <= 1'b0;
			signal_innerProduct <= 1'b0;
			signal_outerProduct <= 1'b0;
			signal_primeDiagonals <= 1'b1;
			i <= 8'd0;
			j <= 8'd0;
			counter <= 32'd0;
			point_count <= -8'd1;
		end
	else
		begin
					
			case(current_state)
				IDLE:
					begin
						counter <= counter + 1;
						if(go)
						begin
							current_state <= calc_primeDiagonals;
						end
					end
				INIT:
					begin
						counter <= counter + 1;
						if(signal_wprime_qhat)
						begin
							qhat[i] <= 32'd0;
							current_state <= calc_qhat_wprime;
						end
						if(signal_qint_qsummand)
						begin
							qint[i] <= 32'd0;
							current_state <= calc_qint_qsummand;
						end
						
					end
				reset_i:
					begin
						counter <= counter + 1;
						i <= 8'd0;
						if(signal_wprime_qhat)
							current_state <= INIT;
						if(signal_qint_qsummand)
							begin
								j <= 8'd0;
								current_state <= INIT;
							end
						if(signal_innerProduct)
							begin
								point_count <= point_count + 1;
								j <= 8'd0;
								current_state <= init_innerproduct;
							end
						if(signal_outerProduct)
							begin
								j <= 8'd0;
								current_state <= init_outerproduct;
							end
					end
				increment_i:
					begin
						counter <= counter + 1;
						i <= i+1;
						j <= 8'd0;
						if(signal_wprime_qhat)
							begin
								current_state <= INIT;
							end
						if(signal_primeDiagonals)
							current_state <= calc_primeDiagonals;
						if(signal_qint_qsummand)
							begin
								current_state <= INIT;
							end
						if(signal_innerProduct)
						begin
							current_state <= init_innerproduct;
						end
						if(signal_outerProduct)
						begin
							current_state <= init_outerproduct;
						end
					end
				increment_j:
					begin
						counter <= counter + 1;
						j <= j+1;
						if(signal_wprime_qhat)
							current_state <= calc_qhat_wprime;
						if(signal_qint_qsummand)
							current_state <= calc_qint_qsummand;
						if(signal_innerProduct)
							current_state <= innerproduct;
						if(signal_outerProduct)
							current_state <= outerproduct;
					end
				calc_primeDiagonals:
					begin
						counter <= counter + 1;
						primeDiagonals[i] <= lastDiagonal/S[i];
						v_inner[i] <= 31'd0;
						if(i < ROWS-1)
							current_state <= increment_i;
						else
							begin
								signal_primeDiagonals <= 1'b0;
								signal_wprime_qhat <= 1'b1;
								current_state <= reset_i;
							end
					end
				calc_qhat_wprime:
					begin
						counter <= counter + 1;
						qhat[i] <= qhat[i] + Uinv[i][j] * q[j];
						Wprime[i][j] <= Winv[j][i] * primeDiagonals[i];
						if(j < COLUMNS-1)
							current_state <= increment_j;
						else if(i < ROWS-1)
							current_state <= increment_i;
						else
							begin
								signal_wprime_qhat <= 1'b0;
								signal_qint_qsummand <= 1'b1;
								current_state <= reset_i;
							end
					end
				calc_qint_qsummand:
					begin
						counter <= counter + 1;
						qint[i] <= qint[i] + Wprime[i][j] * -qhat[j];
						qsummand[i] <= lastDiagonal * q[i];
						if(j < COLUMNS-1)
							current_state <= increment_j;
						else if(i < ROWS-1)
							current_state <= increment_i;
						else
							begin
								signal_qint_qsummand <= 1'b0;
								signal_innerProduct <= 1'b1;
								current_state <= reset_i;
							end
					end
				init_innerproduct:
					begin
						counter <= counter + 1;
						inner <= qint[i];
						current_state <= innerproduct;
					end
				innerproduct:
					begin
						counter <= counter + 1;
						inner <= inner + Wprime[j][i] * v_inner[j];
						if(j < ROWS-1)
							current_state <= increment_j;
						else
						begin
							current_state <= inner_lastDiagonal_mod;
						end
						
					end
				inner_lastDiagonal_mod:
					begin
						counter <= counter + 1;
						inner = inner % lastDiagonal;
						current_state <= inner_lastDiagonal;
					end
				inner_lastDiagonal:
					begin
						counter <= counter + 1;
						if(inner == 0 && o[i] == 1)
								inner <= lastDiagonal;
						current_state <= add_inner;
					end
					
				add_inner:
					begin
						counter <= counter + 1;
						innerRes[i]<= inner;
						if(i < COLUMNS-1)
							current_state <= increment_i;
						else
						begin
							signal_innerProduct <= 1'b0;
							signal_outerProduct <= 1'b1;
							current_state <= reset_i;
						end
					end
				init_outerproduct:
					begin
						counter <= counter + 1;
						outer <= 31'd0;
						current_state <= outerproduct;
					end
				outerproduct:
					begin
						counter <= counter + 1;
						outer <= outer + V[i][j] * innerRes[j];
						if(j < ROWS-1)
							current_state <= increment_j;
						else
						begin
							current_state <= add_outer;
						end	
					end
				add_outer:
					begin
						counter <= counter + 1;
						result[point_count][i]<= (outer + qsummand[i])/lastDiagonal;
						if(i < COLUMNS-1)
							current_state <= increment_i;
						else
						begin
							signal_outerProduct <= 1'b0;
							current_state <= make_j_max;
						end
					end
				make_j_max:
				begin
					counter <= counter + 1;
					j <= ROWS-1;
					current_state <= check_next_vector;
				end
				
				check_next_vector:
				begin
					counter <= counter + 1;
					if(v_inner[j] < S[j]-1)
					begin
						v_inner[j] <= v_inner[j] + 1;
						signal_innerProduct <= 1'b1;
						current_state <= reset_i;
					end
					else 
					begin
						v_inner[j] <= 31'd0;
						current_state <= check_finish;
					end
				end
				
				check_finish:
				begin
					counter <= counter + 1;
					if(j == 8'd0)
						current_state <= END;
					else
					begin
						j <= j-1;
						current_state <= check_next_vector;
					end
				end
				END:
					begin
						if(!go)
							current_state <= IDLE;
					end
					
				

			endcase
		end
	end

endmodule

