
module controlunit(
	input 		xeqy,
	input			xgty,
	input 		go_i,
	input 		reset,
	input 		CLK,
	output reg	ldx,
	output reg  ldy,
	output reg	ldd,
	output reg	Sx,
	output reg	Sy,
	output reg	Ss
);
	
localparam	IDLE 	= 3'd0;
localparam	LOOP 	= 3'd1;
localparam	XBIG 	= 3'd2;
localparam	YBIG 	= 3'd3;
localparam	END 	= 3'd4;	
	
reg	[2:0] current_state;
reg	[2:0] next_state;

//State Register
always@(posedge CLK, posedge reset)
	begin
		if(reset)
			current_state <= 3'd0;
		else
			current_state <= next_state;		
	
	end

	
//Next State Logic & Output Logic
always@(*)
	begin
		next_state = current_state;
		ldx 	= 1'b0;
		ldy 	= 1'b0;
		ldd 	= 1'b0;
		Sx 	= 1'b0;
		Sy 	= 1'b0;
		Ss 	= 1'b0;
		case(current_state)
			
			IDLE:
				begin
					ldx 	= 1'b1;
					ldy 	= 1'b1;
					Sx 	= 1'b0;
					Sy 	= 1'b0;
					if(go_i)
						next_state = LOOP;
				end
			
			LOOP:
				begin
					if(xeqy)
						next_state = END;
					else if(xgty)
						next_state = XBIG;
					else
						next_state = YBIG;
				end
			
			XBIG:
				begin
					Ss 	= 1'b0;
					ldx 	= 1'b1;
					Sx 	= 1'b1;
					next_state = LOOP;
				end
			
			YBIG:
				begin
					Ss		= 1'b1;
					ldy 	= 1'b1;
					Sy 	= 1'b1;
					next_state = LOOP;
				end
			
			END:
				begin
					ldd	= 1'b1;
					if(!go_i)
						next_state = IDLE;
				end
		endcase
				
	end


/*
//Outputs
always@(current_state)
		begin
			ldx 	= 1'b0;
	 		ldy 	= 1'b0;
	 		ldd 	= 1'b0;
	 		Sx 	= 1'b0;
	 		Sy 	= 1'b0;
			Ss 	= 1'b0;
			
			case(current_state)
				IDLE:
					begin
						ldx 	= 1'b1;
						ldy 	= 1'b1;
						Sx 	= 1'b0;
						Sy 	= 1'b0;
					end
							
				XBIG:
					begin
						Ss 	= 1'b0;
						ldx 	= 1'b1;
						Sx 	= 1'b1;
					end
				
				YBIG:
					begin
						Ss		= 1'b1;
						ldy 	= 1'b1;
						Sy 	= 1'b1;
					end
				
				END:
					begin
						ldd	= 1'b1;
					end
			endcase
		end
	
*/	
endmodule
	