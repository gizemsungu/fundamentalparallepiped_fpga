
module gcdstr_tb();

reg 			go_i;
reg 			reset;
reg 	[7:0] A;
reg 	[7:0] B;
reg 			CLK;
wire 	[7:0] Z;


gcdsingle g0(
	.go_i(go_i),
	.reset(reset),
	.x_i(A),
	.y_i(B),
	.CLK(CLK),
	.d_o(Z)
);

initial
	begin
		go_i 	= 1'b0;
		CLK 	= 1'b0;
		reset = 1'b0;
		A 		= 8'd0;
		B 		= 8'd0;
		
		#2
		reset	= 1'b1;
		
		#1
		reset	= 1'b0;
		
		#2
		A 		= 8'd60;
		B 		= 8'd36;
		
		#1
		go_i  = 1'b1;
		
	end


always
	begin
		#5
		CLK = ~CLK;
	end
	
always
	begin
		#10
		$display("state: %d\n", g0.current_state);
		$display("x: %d\n", g0.x);
		$display("y: %d\n", g0.y);
		$display("****************\n");
	end
	
endmodule

