
module datapath(
	input 	ldx,
	input 	ldy,
	input 	ldd,
	input 	Sx,
	input 	Sy,
	input		Ss,
	input		CLK,
	input		[7:0] x_i,
	input		[7:0] y_i,
	output	[7:0] d_o,
	output 	xeqy,
	output	xgty
	);


wire	[7:0] xval;	
wire 	[7:0] yval;
wire	[7:0] xsuby;
wire	[7:0] ysubx;
wire	[7:0] subA;
wire	[7:0] subB;
wire  [7:0] subR;

reg	[7:0] x;
reg	[7:0] y;
reg	[7:0] d;

assign d_o = d;
assign xval = Sx ? subR : x_i;
assign yval = Sy ? subR : y_i;
assign subA = Ss ? y : x;
assign subB = Ss ? x : y;
assign subR = subA - subB;
assign xeqy = x==y ? 1'b1 : 1'b0;
assign xgty = x>y ? 1'b1 : 1'b0;


always@(posedge CLK)
	begin
		if(ldx)
			x <= xval;
		if(ldy)
			y <= yval;
		if(ldd)
			d <= x;
	end
	
	
endmodule
