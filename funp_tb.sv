
module funp_tb();


parameter ROWS = 3,
  		NUM_POINTS = 2,
  		 COLUMNS = 3;

reg [31:0] V [0:ROWS-1][0:COLUMNS-1];
reg [31:0] q [0:2];
reg [7:0] o [0:2];
reg [31:0] Uinv [0:ROWS-1][0:COLUMNS-1];
reg [31:0] Winv [0:ROWS-1][0:COLUMNS-1];
reg [31:0] S [0:COLUMNS-1];
reg		reset;
reg		CLK;
reg	go;
wire [31:0] counter;
wire [31:0] result [0:NUM_POINTS-1][0:COLUMNS-1];


funp #( .ROWS(ROWS),
			.NUM_POINTS(NUM_POINTS),
			.COLUMNS(COLUMNS)) fun(
	.V(V),
	.q(q),
	.o(o),
	.Uinv(Uinv),
	.Winv(Winv),
	.S(S),
	.CLK(CLK),
	.go(go),
	.reset(reset),
	.counter(counter),
	.result(result)
);

initial
	begin
		CLK 	= 1'b0;
		go  = 1'b0;
		reset = 1'b1;
		V[0][0] = 32'd0;
		V[0][1] = 32'd0;
		V[0][2] = 32'd1;
		V[1][0] = 32'd0;
		V[1][1] = 32'd2;
		V[1][2] = 32'd2;
		V[2][0] = 32'd1;
		V[2][1] = 32'd3;
		V[2][2] = 32'd3;
		q[0] = 32'd1;
		q[1] = 32'd2;
		q[2] = 32'd3;
		o[0] = 8'd0;
		o[1] = 8'd0;
		o[2] = 8'd0;
		
		Uinv[0][0] = 32'd0;
		Uinv[0][1] = 32'd0;
		Uinv[0][2] = 32'd1;
		Uinv[1][0] = 32'd1;
		Uinv[1][1] = 32'd0;
		Uinv[1][2] = 32'd0;
		Uinv[2][0] = 32'd2;
		Uinv[2][1] = -32'd1;
		Uinv[2][2] = 32'd0;
		
		Winv[0][0] = 32'd1;
		Winv[0][1] = -32'd3;
		Winv[0][2] = 32'd3;
		Winv[1][0] = 32'd0;
		Winv[1][1] = 32'd0;
		Winv[1][2] = -32'd1;
		Winv[2][0] = 32'd0;
		Winv[2][1] = 32'd1;
		Winv[2][2] = 32'd0;
		
		S[0] = 32'd1;
		S[1] = 32'd1;
		S[2] = 32'd2;
		
		#3
		reset	= 1'b0;
		
		#4
		go  = 1'b1;
		
	end


always
	begin
		#5
		CLK = ~CLK;
	end
	
always
	begin
		#10
		$display("result: %p\n", fun.result);
		$display("state: %d\n", fun.current_state);
		$display("qhat: %p\n", fun.qhat);
		$display("qint: %p\n", fun.qint);
		$display("qsummand: %p\n", fun.qsummand);
		$display("innerRes: %p\n", fun.innerRes);
		$display("wPrime: %p\n", fun.Wprime);
		$display("v inner: %p\n", fun.v_inner);
		$display("counter: %d\n", fun.counter);
		$display("****************\n");
	end
	
endmodule

