module gcdsingle(

	input					go_i,
	input					reset,
	input			[7:0]	x_i,
	input			[7:0]	y_i,
	input					CLK,
	output reg	[7:0]	d_o
);


localparam	IDLE 	= 3'd0;
localparam	LOOP 	= 3'd1;
localparam	XBIG 	= 3'd2;
localparam	YBIG 	= 3'd3;
localparam	END 	= 3'd4;	

reg	[2:0]	current_state;


reg [7:0] x;
reg [7:0] y;



//State Register & other registers
always@(posedge CLK, posedge reset)
	begin
	if(reset)
		begin
			current_state <= IDLE;
			x <= 8'd0;
			y <= 8'd0;
			d_o <= 8'd0;
			
		end
	else
		begin
					
			case(current_state)
				IDLE:
					begin
						x <= x_i;
						y <= y_i;
						if(go_i)
							current_state <= LOOP;
					end
			
				LOOP:
					begin
													
						if(x == y)
							current_state <= END;
						else if(x > y)
							current_state <= XBIG;
						else
							current_state <= YBIG;
					end
				
				XBIG:
					begin
						x <= x - y;
						current_state = LOOP;
					end
				
				YBIG:
					begin
						y <= y - x;
						current_state = LOOP;
					end
				
				END:
					begin
						d_o <= x;
						if(!go_i)
							current_state = IDLE;
					end
			endcase
		end
	end

/*
//Datapath & Next State Logic
always@(*)
	
	begin
		xeqy 	= 1'b0;
		xgty 	= 1'b0;
		ldx 	= 1'b0;
		ldy 	= 1'b0;
		ldd 	= 1'b0;
		next_state = current_state;
		
		case(current_state)
			
			IDLE:
				begin
					xval = x_i;
					yval = y_i;
					ldx = 1'b1;
					ldy = 1'b1;
					
					if(go_i)
						next_state = LOOP;
				end
			
			LOOP:
				begin
					if(x == y)
						xeqy = 1'b1;
					else if(x > y)
						xgty = 1'b1;
						
					if(xeqy)
						next_state = END;
					else if(xgty)
						next_state = XBIG;
					else
						next_state = YBIG;
				end
			
			XBIG:
				begin
					ldx 	= 1'b1;
					xval 	= x - y;
					next_state = LOOP;
				end
			
			YBIG:
				begin
					ldy 	= 1'b1;
					yval 	= y - x;
					next_state = LOOP;
				end
			
			END:
				begin
					ldd = 1'b1;
					if(!go_i)
						next_state = IDLE;
				end
		endcase
	end
	*/



endmodule

