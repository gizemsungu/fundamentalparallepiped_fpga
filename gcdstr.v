
module gcdstr(

	input				go_i,
	input				reset,
	input		[7:0]	x_i,
	input		[7:0]	y_i,
	input				CLK,
	output	[7:0]	d_o
);


wire ildx;
wire ildy;
wire ildd;
wire iSx;
wire iSy;
wire iSs;
wire ixeqy;
wire ixgty;

controlunit c0(
	 		.xeqy(ixeqy),
			.xgty(ixgty),
	 		.go_i(go_i),
	 		.reset(reset),
	 		.CLK(CLK),
	 		.ldx(ildx),
	 		.ldy(ildy),
	 		.ldd(ildd),
	 		.Sx(iSx),
	 		.Sy(iSy),
			.Ss(iSs)
);

datapath d0(
	 	.ldx(ildx),
	 	.ldy(ildy),
	 	.ldd(ildd),
	 	.Sx(iSx),
	 	.Sy(iSy),
		.Ss(iSs),
		.CLK(CLK),
		.x_i(x_i),
		.y_i(y_i),
		.d_o(d_o),
	 	.xeqy(ixeqy),
		.xgty(ixgty)
);




endmodule

	
	